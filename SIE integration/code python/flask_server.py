
from flask import Flask, render_template, request, jsonify
import xmlrpc.client
import requests

app = Flask(__name__)

# Informations de connexion à Odoo
url = "https://unige-sie.odoo.com"
db = "unige-sie"
username = "daniel.pinto.1@etu.unige.ch"
password = "R'3QkN}5L^uFPtB"

# Informations de l'API Typeform
API_KEY = 'tfp_DfB8VJfetc78u66o1VDwzFPk7WdGFaH44MF78YBsiV5t_3pf2G2Y1BgX5k9'
FORM_ID = 'tSGqkI9X'
TYPEFORM_ENDPOINT = f"https://api.typeform.com/forms/{FORM_ID}/responses"

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/process-form', methods=['POST'])
def process_form():
    headers = {'Authorization': f'Bearer {API_KEY}'}
    params = {'page_size': 1, 'sort': 'submitted_at,desc'}
    response = requests.get(TYPEFORM_ENDPOINT, headers=headers, params=params)

    if response.status_code == 200:
        data = response.json()
        answer_texts = []

        for item in data['items']:
            for answer in item['answers']:
                if answer['type'] == 'text':
                    answer_texts.append(answer['text'])
                elif answer['type'] == 'choice':
                    answer_texts.append(answer['choice']['label'])

        common = xmlrpc.client.ServerProxy(f'{url}/xmlrpc/2/common')
        uid = common.authenticate(db, username, password, {})
        models = xmlrpc.client.ServerProxy(f'{url}/xmlrpc/2/object')

        if "Remboursement" in answer_texts[6]:
            order_id = int(answer_texts[2])
            models.execute_kw(db, uid, password, 'sale.order', 'write', [[order_id], {'state': 'cancel'}])
            return jsonify({'message': f"Commande {order_id} annulée."})

        elif "Remplacement par un nouveau produit" in answer_texts[6]:
            product_id = int(answer_texts[3])
            customer_name = answer_texts[0]
            new_order_id = models.execute_kw(db, uid, password, 'sale.order', 'create', [{
                'state': "draft",
                'partner_id': 3,
                'order_line': [(0, 0, {'product_id': product_id, 'product_uom_qty': 1})],
            }])
            models.execute_kw(db, uid, password, 'sale.order', 'action_confirm', [[new_order_id]])
            return jsonify({'message': f"Nouvelle commande {new_order_id} créée pour {customer_name}."})
            

    return jsonify({'error': 'Failed to fetch responses or process data'}), 400

if __name__ == '__main__':
    app.run(debug=True)
